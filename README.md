### ========= This is to setup the M1 Odroid TwisterOS UI and Apps =======

#### Pre

Before you start need to make sure latest Ansible is installed 

```bash
sudo apt update 
sudo apt install ansible 
```

Then clone the repo 

```bash
git clone https://gitlab.com/terrorpup/twisterosm1setup.git
``` 

### ======================================================================

#### Before running 

The default user is m1, the playbook will setup the user.

---
- vars:<br> 
    uusername: m1<br>
    upassword: Password1<br> 
---

Change **uusername** to the user that is wanted. 

Change **upassword** to the password for the username. 


### ======================================================================

### Setup Odroid TwisterOS 

```bash 
ansible-playbook m1setup.yml
``` 
### ======================================================================

The playbook will make sure that XFCE4 is installed. 

The playbook will install the following applications:
- bluefish
- box64
- caffine
- chorium
- conky
- dolphin
- dosbox 
- flameshot
- galculater
- geany
- gimp
-  hexchat
- htop
- kazam
- kodi
- jstest-gtk
- mc
- mednaffe
- monodoc-http
- mpv
- onboard
- parole
- plank
- qjoypad
- remmina
- retroarch
- snap
- snapd
- snummvm
- smplayer
- thunderbird
- thunar
- tmux
- transmission
- viewnior
- vlc ssh 
    

### ====================================================================== 

#### Debian OS firmware

This playbook will install the following kernel packages to help add
support for other wireless devices. 

It will adjust the following file 

```bash
/etc/apt/sources.list 
``` 

It will add the following **contrib non-free** behind **main**: 
- Atheros Firmware
- Broadcom Firmware
- Realtek Firmware
- Linux Free Firmware
- Linux Non-Free Firmware 
- Misc Non-Free Firmware 

This has been test with Debian 10 and 11. 

To use with Ubuntu comment out the following line m1setup.yml 

```python
   when: ansible_os_family == "Debian"
```

### ====================================================================== 

#### Snap Apps 

It will install the following Snap Apps:
- bottom   

### ======================================================================

Twister UI GitHub Repo 

[Layquack's GitHub Twister UI](https://github.com/layquark/Twister-UI.git) 

### ======================================================================
